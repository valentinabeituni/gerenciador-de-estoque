from django.apps import AppConfig

from stock.models import Product

class stockConfig(AppConfig):
    name = 'stock'
